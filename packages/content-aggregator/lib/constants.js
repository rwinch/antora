'use strict'

module.exports = Object.freeze({
  COMPONENT_DESC_FILENAME: 'antora.yml',
  CONTENT_CACHE_FOLDER: 'content',
  CONTENT_GLOB: '**/*.*',
})
