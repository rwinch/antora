= Windows Installation Requirements
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// URLs
:url-choco: https://chocolatey.org
:url-nvm-windows: https://github.com/coreybutler/nvm-windows
// Versions:
:version-node: 8.10.0

On this page, you'll learn:

* [x] What tools you need in order to install Antora on Windows.
* [x] How to install Node 8.
* [x] How to install Chocolatey, the Windows package manager.

If you've never installed Antora before, you need to complete the steps on this page before you can generate a documentation site.

To install Antora, you need Node 8 and Chocolatey.

== Node 8

Antora requires Node 8, the current long term support (LTS) release of Node.
While you can try Node 9, Antora is not currently tested on it.

To check which version of Node you have installed, if any, open PowerShell and type:

[source]
$ node --version

*If the command fails with an error*, you don't have Node installed.
The best way to install Node 8 is via Chocolatey.
If you don't have Chocolatey on your machine, go to <<install-choco,install Chocolatey>>, otherwise, skip directly to <<install-nvm,install nvm and Node>> for instructions.

*If the command returns a version less than 8.0.0*, upgrade to the latest Node 8 version using nvm.
Go directly to <<upgrade-node,upgrade Node>> for instructions.

*If the command returns a Node 8 version*, you're ready to xref:install/install-antora.adoc[install Antora].

[#install-choco]
== Install Chocolatey

The best way to install the Node Version Manager (nvm) and Node is with {url-choco}[Chocolatey^], the package manager for Windows.

. Open a PowerShell terminal and run it as an Administrator by right clicking on the PowerShell icon and selecting menu:Run as Administrator[].

. Type the following command in the terminal:
+
[source]
$ Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

You're now ready to install nvm and Node.

[#install-nvm]
== Install nvm and Node

TIP: If you just installed Chocolatey using the instructions in the proceeding section, use the terminal, running as Administrator, you already have open.

. Open a PowerShell terminal, right click on the PowerShell icon, and select menu:Run as Administrator[].

. To install the {url-nvm-windows}[Node Version Manager (nvm) for Windows^], enter the following command in the terminal:
+
[source]
$ choco install -y nvm

. Close the terminal.

. Open an new, regular PowerShell terminal, and install Node with nvm.
+
[source,subs=attributes+]
$ nvm install {version-node}
+
IMPORTANT: When using nvm for Windows, you must enter the full version of Node (i.e., `nvm install {version-node}`) until {url-nvm-windows}/issues/214[nvm-windows#214^] is resolved.

Now that Node is installed, you're ready to xref:install/install-antora.adoc[install Antora].

.nvm and CI environments
****
You can install the LTS release of Node directly, without installing nvm, by entering the following command in the Administrator PowerShell:

[source]
$ choco install -y nodejs-lts

However, many CI environments use nvm to install the Node version used for the build job.
By using nvm, you closely align your setup with the environment that is used to generate and publish your production site.
****

[#upgrade-node]
== Upgrade Node with nvm

If you have nvm installed but your Node version is less than 8.0.0, type the following command in your terminal to install the latest version of Node 8:

[source,subs=attributes+]
$ nvm install {version-node}

IMPORTANT: When using nvm for Windows, you must enter the full version of Node (i.e., `nvm install {version-node}`) until {url-nvm-windows}/issues/214[nvm-windows#214^] is resolved.

Now that you've upgraded Node, you're ready to xref:install/install-antora.adoc[install Antora].

== What's next?

Once you've installed Node 8, it's time to xref:install/install-antora.adoc[install Antora].
